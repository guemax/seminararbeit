\chapter{Grundlagen der Graphentheorie}

Bevor wir uns dem eigentlichen Thema dieser Arbeit, dem
Kürzeste-Wege-Problem widmen können, müssen wir einige Grundlagen der
Graphentheorie behandeln.

Dieses Kapitel soll lediglich der Übersicht dienen und Begriffe, die
im weiteren Verlauf der Seminararbeit auftreten, erklären.  Für einen
tiefergehenden Einblick ist das Buch \citetitle{die96} von
\citefullauthor{die96} zu empfehlen.

\section{Graphen}

Ein \itnote{Graph} \(G = (V,E)\) ist ein Paar zweier Mengen \(V\)
und \(E\).  \enquote{Die Elemente von \(V\) nennt man die
  \itnote{Ecken} (oder \textit{Knoten}) des Graphen \(G\), die
  Elemente von \(E\) seine \itnote{Kanten}} \autocite[2]{die96}.

Eine Kante \(e = \{x,y\}\) \textit{verbindet} die beiden Ecken \(x\)
und \(y\).  Kurz schreiben wir auch nur \(xy\) (oder \(yx\)).

Wir sagen, \(G = (V,E)\) ist ein Graph \itnote{auf} \(V\)
\autocite[siehe][2]{die96}.

Zur Visualisierung des Graphen werden in der Regel Punkte für die
Ecken und Linien für die Kanten verwendet.  Die Darstellung des
Graphen ist jedoch unabhängig von seiner formellen Notation, und ist
meist eher \enquote{eine Frage der Zweckmäßigkeit und der Ästhetik}
\autocite[2]{die96}.

Ein möglicher Graph sowie seine Darstellung ist in
\autoref{fig:graphentheorie-beispielgraph} zu sehen.

\begin{figure}[h]
  \centering
  \includegraphics{graphentheorie-beispielgraph}
  \caption[Darstellung eines Graphen]{Der Graph auf
    \(V = \{1,\ldots,8\}\) mit der Kantenmenge
    \(E = \{\{1,2\},\{2,6\},\)\\\(\{6,7\},\{7,1\},\{5,8\}\}\).}
  \label{fig:graphentheorie-beispielgraph}
\end{figure}

Wir bezeichnen eine Ecke \(v\) als \itnote{inzident} mit einer Kante
\(e\), wenn \(v \in e\) gilt.  \enquote{Die beiden mit einer Kante
  \(e\) inzidenten Ecken sind ihre \textit{Endecken}}
\autocite[2]{die96}.  Zwei Ecken werden \itnote{adjazent} (oder
\textit{benachbart}) genannt, wenn sie durch eine Kante \(xy\)
verbunden werden \autocite[siehe][3]{die96}.

Zusätzlich zu oben genanntem allgemeinen Graphen sind für diese
Seminararbeit noch zwei weitere Arten von großer Bedeutung:

\itnote[gerichtet]{Gerichtete Graphen} besitzen Kanten, die nur in
einer bestimmten Richtung durchlaufen werden können.  Oft werden diese
Kanten dann \itnote{Bögen} genannt.  In der Praxis können Bögen zum
Beispiel Einbahnstraßen darstellen \autocite[siehe][25]{die96}.

\itnote[gewichtet]{Gewichtete Graphen} ordnen jeder Kante
beziehungsweise jedem Bogen eine reelle Zahl zu.  Diese Zahl
bezeichnen wir als \textit{Kanten-} oder \itnote{Bogengewicht}.  Das
Gewicht einer Kante oder eines Bogens kann unter anderem die Fahrzeit
in Minuten, die Entfernung in Kilometern, aber auch die Kosten oder
Kapazitäten einer Verbindung ausdrücken
\autocite[siehe][36--37]{gri05}.

Da Graphen häufig zugleich gerichtet und gewichtet sind, sprechen wir
von \itnote[Gewichteter Digraph]{gewichteten Digraphen}\footnote{Aus
  dem Englischen: \enquote{directed} gleich \enquote{gerichtet}.}
statt von gewichteten gerichteten Graphen, um Verwechslungen
vorzubeugen.

\section{Der Grad einer Ecke}

Die Anzahl der mit einer Ecke \(v\) inzidenten Kanten wird als
\itnote{Grad} \(d_G(v)\) bezeichnet.  Eine Ecke des Grades 0 ist
\itnote{isoliert} \autocite[siehe][5]{die96}.

Mit dem \itnote[Ingrad\\Ausgrad]{Ingrad} \(d_{in}(v)\) beziehungsweise
dem \textit{Ausgrad} \(d_{aus}(v)\) wird die Anzahl der Kanten
angegeben, die zur Ecke \(v\) hin- beziehungsweise von ihr wegführen.

Ein paar Beispiele für den In- und Ausgrad einer Ecke sind in
\autoref{fig:graphentheorie-in-und-ausgrade} zu sehen.

\begin{figure}[h]
  \centering
  \subcaptionbox{\(\text{Ingrad} = 1,\ \text{Ausgrad} =
    1\)}{\centering\includegraphics{graphentheorie-ingrad-eins-ausgrad-eins}}
  \subcaptionbox{\(\text{Ingrad} = 3,\ \text{Ausgrad} =
    1\)}{\centering\includegraphics{graphentheorie-ingrad-drei-ausgrad-eins}}
  \subcaptionbox{\(\text{Ingrad} = 2,\ \text{Ausgrad} =
    1\)}{\centering\includegraphics{graphentheorie-ingrad-zwei-ausgrad-eins}}
  \caption[In- uns Ausgrad einer Ecke]{Beispiele für den In- und
    Ausgrad einer Ecke.}
  \label{fig:graphentheorie-in-und-ausgrade}
\end{figure}

\section{Wege und Kreise}
\label{sec:graphentheorie-wege-und-kreise}

Als \itnote{Weg} bezeichnen wir einen nichtleeren Graphen
\(P = (V,E)\) der Form

\begin{equation*}
  V = \{x_0,x_1,\ldots,x_k\} \qquad E = \{x_0x_1,x_1x_2,\ldots,x_{k-1}x_k\}.
\end{equation*}

Die \enquote{Ecken \(x_0\) und \(x_k\) sind die \textit{Endecken} von
  \(P\), die Ecken \(x_1,\ldots,x_{k-1}\) seine \textit{inneren Ecken}}
\autocite[6]{die96}.  Die \itnote{Länge} \(P^k\) des Weges \(P\)
entspricht der Anzahl seiner Kanten.

\enquote{Ist \(P = x_0 \ldots x_{k-1}\) ein Weg und \(k \geq 3\), so ist
  der Graph \(C := P + x_{k-1}x_0\) ein \itnote{Kreis}}
\autocite[8]{die96}.  Seine Länge wird mit \(C^k\) bezeichnet.

Der \itnote{Abstand} \(d_G(x,y)\) ist die \enquote{geringste Länge
  eines \(x\)-\(y\)-Weges in \(G\)} \autocite[9]{die96}.  Existiert
kein solcher Weg, so bedienen wir uns eines kleinen Tricks und
setzen \(d_G(x,y) := \infty\).

Durch diesen Trick müssen wir bei der Berechnung eines kürzesten Weges
nicht unterscheiden, ob ein Weg existiert oder nicht, und vereinfachen
dadurch die Algorithmen.

\section{Euler- und Hamiltonwege}

Ein Weg in einem Graphen wird \itnote{Eulerweg}
genannt, wenn er \enquote{jede Kante des Graphen genau einmal enthält}
\autocite[19]{die96}.

Ist zusätzlich zu obiger Bedingung die Startecke gleich der Endecke,
so spricht man von einem \itnote{Eulerkreis}
\autocite[siehe][190]{gri05} und nennt den Graphen \itnote{eulersch}
\autocite[siehe][57]{gri13}.

Analog zum Eulerweg heißt ein Weg \itnote{Hamiltonweg} beziehungsweise
ein Kreis \itnote{Hamiltonkreis}, wenn er jede Ecke des Graphen genau
einmal enthält.  Ein Graph wird \itnote{hamiltonsch}
genannt, wenn er einen Hamiltonkreis besitzt
\autocite[siehe][56--57]{gri13}.

Beispiele für einen Eulerweg und einen Hamiltonkreis sind in
\autoref{fig:graphentheorie-eulerweg-und-hamiltonkreis} zu sehen.

\begin{figure}[h]
  \centering
  \subcaptionbox{Das Haus des
    Nikolaus}[.4\textwidth]{\centering\includegraphics{graphentheorie-das-ist-das-haus-vom-nikolaus}}
  \subcaptionbox{Ein
    Dodekaeder}[.4\textwidth]{\centering\includegraphics{graphentheorie-dodekaeder}}
  \caption[Eulerweg und Hamiltonkreis]{Links: Beispiel eines
    Eulerweges.  Rechts: Beispiel eines Hamiltonkreises.}
  \label{fig:graphentheorie-eulerweg-und-hamiltonkreis}
\end{figure}

Gerade in der Logistik sind Euler- und Hamiltonkreise von großer
Bedeutung.  Mehr dazu jedoch in
\autoref{sec:anwendungen-chinesisches-postboten-problem}.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../seminararbeit"
%%% End:
