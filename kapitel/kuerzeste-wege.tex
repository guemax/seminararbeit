\chapter{Kürzeste Wege in einem Graphen}

Das Problem, einen kürzesten Weg zwischen den Ecken \(s\) und \(t\) zu
finden, wird \textit{Kürzeste-Wege-Problem}, abkürzend auch SPP oder
SP-Problem\footnote{Aus dem Englischen: \enquote{shortest path
    problem}.}, genannt
\autocites[siehe][13]{gri05}[siehe][154]{gri13}.

Bei einer Aufgabe kürzester Wege sind ein allgemeiner Graph
\(G = (V,E)\) und eine Gewichtsfunktion
\(\phi: E \mapsto \mathbb{R}\), die jeder Kante ein reelles Gewicht
zuordnet, gegeben.

Das Gewicht \(\varphi(P)\) des Weges \(P = \{v_0,v_1,\ldots,v_k\}\) ist
die Summe aller Kantengewichte, die die in \(P\) enthaltenen Ecken
verbinden:
\begin{equation*}
  \varphi(P) = \sum_{i=1}^{k} \phi(v_{i-1},v_i)
\end{equation*}

Ziel ist es, \(\varphi(P)\) zu minimieren
\autocites[siehe][154]{gri13}[siehe][643]{cor09}.

\section{Die naive Methode}

Schnell möchte man meinen, dass das stupide Ausprobieren aller
möglichen \(s\)-\(t\)-Wege eine akzeptable, wenn auch nicht optimale
Methode sei.  Sehen wir uns folgenden, stark vereinfachten Graphen mit
vier Schichten \autocite[aus ][44--46]{gri05} an:
\begin{center}
  \includegraphics[height=.15\textheight]{kuerzeste-wege-vereinfachter-graph-mit-vier-schichten}
\end{center}

Gehen wir von \(s\) aus los, so haben wir zwei Möglichkeiten, um zur
ersten Schicht zu gelangen: Entweder wählen wir die obere Kante oder
die untere für unseren Weg.  Ist man nun an einer der Ecken in der
ersten Ebene angelangt, so hat man wieder zwei Möglichkeiten, zu einer
der Ecken der zweiten Ebene zu gelangen, und so weiter.

Allgemein lässt sich festhalten, dass wir für Graphen ähnlich dem
obigen mit \(n\) Schichten \(2^n\) mögliche Wege erhalten, deren Länge
wir jeweils berechnen und vergleichen müssen, um den kürzesten Weg zu
erhalten.

In unserem Beispiel mit vier Schichten sind dies \(2^4 = 16\) Wege.
Selbst händisch dürfte es relativ einfach sein, in diesem Graphen --
auch wenn seine Kanten mit Gewichten versehen sind -- einen kürzesten
Weg zu finden.

Fügen wir nun eine weitere Schicht hinzu, so kommen wir schon auf
\(2^5 = 32\) mögliche Wege.  Der Graph sieht nun wie folgt aus:
\begin{center}
  \includegraphics[height=.15\textheight]{kuerzeste-wege-vereinfachter-graph-mit-fuenf-schichten}
\end{center}

Auch hier dürfte man schnell zu einer Lösung gelangen.  In der realen
Welt ist ein Graph jedoch selten so einfach wie in unserem Beispiel,
oft sind hier Graphen mit \enquote{Tausenden von Knoten}
\autocite[46]{gri05} im Spiel.  Aus diesem Grund wollen wir nun das
Beispiel auf 50 Schichten (insgesamt \enquote{nur} 102 Ecken)
erweitern:
\begin{center}
  \includegraphics[height=.15\textheight]{kuerzeste-wege-vereinfachter-graph-mit-fuenfzig-schichten}
\end{center}

Damit kommen wir nun schon auf
\begin{equation*}
  2^{50} = \num{1125899906842624},
\end{equation*}
also mehr als eine Billiarde mögliche Wege.  Sollte ein moderner
Computer nicht weiterhin in der Lage sein, hier vergleichsweise
schnell einen kürzesten Weg zu berechnen?  \citefullauthor{gri05} gehen in
ihrem Buch \citetitle{gri05} davon aus, dass ein \qty{1}{\GHz}
Computer in der Lage ist, \enquote{in jeder Sekunde eine Million Wege
  durchtesten [zu können]} \autocite[46]{gri05}.

Seit der Veröffentlichung dieses Buches im Jahr 2005 hat sich
bezüglich der Computertechnik viel getan.  Heutzutage ersetzen
Kartenapps auf dem Smartphone sogar schon das Navi im Auto.  Für die
Betrachtung der Laufzeit unserer naiven Herangehensweise soll darum
das Google Pixel~8 verwendet werden, da das Google Pixel in der Welt
der mobilen Endgeräte meist als Referenzmodell für Software und
Benchmarks dient.

Es besitzt einen \qty{3}{\GHz} Prozessor
\autocite[siehe][110]{por23}.  Rechnen wir Gritzmanns Annahme
hoch, so sind wir mit dem Google Pixel~8 in der Lage, ganze
\num{3000000} Wege pro Sekunde zu vergleichen.

Damit benötigen wir für unseren 50-schichtigen Graphen insgesamt
\begin{equation*}
  \frac{\num{1125899906842624}}{\num{3000000}} \: \unit{s} \approx \qty{375299969}{\s},
\end{equation*}
also fast 12 Jahre für die Berechnung des kürzesten Weges!  Für
\enquote{nur wenige neue Knoten ist die Anzahl der Wege regelrecht
  \enquote{explodiert}} \autocite[48]{gri05}.  In der Mathematik wird
dies \textit{kombinatorische Explosion} genannt.

Nachdem wir uns noch immer nicht annähernd in der Größenordnung von
Graphen bewegen, wie sie im Alltag auftreten, und niemand derart lange
warten kann, bis sein Navi eine Route zwischen München und Hamburg
ausgibt, muss eine bessere Lösungsmethode gefunden werden.

\section{Der Dijkstra-Algorithmus}

Ein sehr bekannter Algorithmus zur Lösung des Kürzeste-Wege-Problems
ist der Dijkstra-Algorithmus von Edsger Wybe Dijkstra.  Der
niederländische Mathematiker erfand diesen 1959 und legte damit die
Grundlage für eine Vielzahl weiterer Algorithmen
\autocite[siehe][65]{bag20}.

Wie im vorherigen Fallbeispiel zu sehen, ist es sehr aufwändig, auf
der Suche nach der besten Lösung alle Möglichkeiten durchzurechnen.
In der Regel reichen jedoch schon Methoden mit geringem Aufwand aus
\autocite[siehe][414]{cor09}.

Ein \textit{Greedy-Algorithmus}, zu der auch der von Dijkstra gehört,
trifft immer die momentan beste Entscheidung.  Konkret bedeutet dies,
dass wir bei unserer Berechnung immer die adjazente Ecke wählen, deren
Kantengewicht am kleinsten ist, in der Hoffnung, dass diese lokalen
Entscheidungen zu einer global optimalen Lösung führen
\autocites[siehe][414]{cor09}[siehe][56--58]{gri05}.  Dies muss jedoch
nicht immer der Fall sein.

Bevor wir die Funktionsweise des Dijkstra-Algorithmus Schritt für
Schritt durchgehen, hier ebendieser in seiner Gänze \autocites[siehe
auch][75]{gri05}[oder][658]{cor09}:

\begin{codebox}
  \Procname{\(\proc{Dijkstra}(G,s)\)}
  \li \(S \gets \{s\}, \; \proc{Distanz}(s) \gets 0\)
  \li \For \textbf{all} \(v \in V \setminus \{s\}\)
    \li \Do
    \(\proc{Distanz}(v) \gets \proc{Bogenlänge}(s,v)\)
    \li \(\proc{Vorgänger}(v) \gets s\)
  \End
  \li \While \(S \neq V\)
    \li \Do
    finde \(v*\) aus \(V \setminus S\) mit
    \zi \(\quad \proc{Distanz}(v*) \gets \min\{\proc{Distanz}(v) : v \in V \setminus S\}\)
    \li \(S \gets S \cup \{v*\}\)
    \li \For \textbf{all} \(v \in V \setminus S\)
      \li \Do
      \If \(\proc{Distanz}(v*) + \proc{Bogenlänge}(v*, v) < \proc{Distanz}(v)\)
        \li \Then
        \(\proc{Distanz}(v) \gets \proc{Distanz}(v*) + \proc{Bogenlänge}(v*, v)\)
        \li \(\proc{Vorgänger}(v) \gets v*\)
      \End
    \End
  \End
\end{codebox}

Als Eingabe nimmt der Dijkstra-Algorithmus einen gewichteten Digraphen
\(G = (V,E)\) mit nichtnegativen Kantengewichten sowie eine
Start\-ecke \(s\) an und gibt \textit{einen}\footnote{Nicht jedoch
  \textit{den} kürzesten Weg, da es auch mehrere geben kann.}
kürzesten Weg als Ausgabe zurück \autocite[siehe][658]{cor09}.

Beginnen wir mit der ersten Zeile des Algorithmus:

\begin{codebox}
  \zi \(S \gets \{s\}, \; \proc{Distanz}(s) \gets 0\)
\end{codebox}

Mit \(S\) bezeichnen wir die Menge aller Ecken, zu denen wir bereits
einen kürzesten Weg kennen.  Wir initialisieren \(S\) anfangs mit
\(\{s\}\).

Unter der \proc{Distanz}\((v\)) (sprich \enquote{Distanz von \(v\)})
wird immer das Gewicht \enquote{eines zum aktuellen Zeitpunkt
  kürzesten Weges von \(s\) nach \(v\) gespeichert}
\autocite[67]{gri05}.  Das Gewicht des Weges zwischen \(s\) und \(s\)
selbst ist verständlicherweise 0, wir weisen der Distanz von \(s\)
darum diesen Wert zu.

\begin{codebox}
  \zi \For \textbf{all} \(v \in V \setminus \{s\}\)
    \zi \Do
    \(\proc{Distanz}(v) \gets \proc{Bogenlänge}(s,v)\)
    \zi \(\proc{Vorgänger}(v) \gets s\)
  \End
\end{codebox}

In diesem Schritt gehen wir jede Ecke \(v\) des Graphen~-- bis auf
\(s\)~-- durch.  In der Informatik nennt sich dieser Vorgang
\textit{Schleife}.  Für jede Ecke \(v\) berechnen wir nun dessen
Distanz wie folgt:

Gibt es eine Kante und somit eine direkte Verbindung zwischen \(s\)
und \(v\), so gibt die \proc{Bogenlänge}\((s,v)\) einfach das Gewicht
des Bogens zwischen \(s\) und \(v\) zurück.

Existiert jedoch keine direkte Kante, die die beiden Ecken verbindet,
so gibt die \proc{Bogenlänge}\((s,v)\) den Wert unendlich zurück.
\enquote{Es macht [für den Algorithmus] keinen Unterschied, ob der
  Knoten nicht erreichbar, oder ob er unendlich weit weg ist}
\autocite[69]{gri05}.  Mit Hilfe dieses kleinen Tricks muss jedoch
nicht ständig unterschieden werden, ob ein Bogen existiert oder nicht.
Die \proc{Bogenlänge} verhält sich also ähnlich wie der Abstand
\(d_G(x,y)\) aus \autoref{sec:graphentheorie-wege-und-kreise}.

Der \proc{Vorgänger}\((v)\) speichert für uns den \enquote{direkten
  Vorgängerknoten von \(v\) auf dem bisher bekannten kürzesten Weg
  nach \(v\)} \autocite[69]{gri05}.  Sind wir an der Endecke
angelangt, so lesen wir zunächst den Vorgänger der Endecke aus, darauf
den Vorgänger des Vorgängers der Endecke und so weiter, bis wir bei
der Startecke angelangt sind.  So kennen wir nicht nur das Gewicht des
Weges, sondern auch dessen Verlauf.

Nun kommen wir zum Herzstück des Dijkstra-Algorithmus:

\begin{codebox}
  \zi \While \(S \neq V\)
    \zi \Do
    finde \(v*\) aus \(V \setminus S\) mit
    \zi \(\quad \proc{Distanz}(v*) \gets \min\{\proc{Distanz}(v) : v \in V \setminus S\}\)
    \zi \(S \gets S \cup \{v*\}\)
    \zi \For \textbf{all} \(v \in V \setminus S\)
      \zi \Do
      \If \(\proc{Distanz}(v*) + \proc{Bogenlänge}(v*, v) < \proc{Distanz}(v)\)
        \zi \Then
        \(\proc{Distanz}(v) \gets \proc{Distanz}(v*) + \proc{Bogenlänge}(v*, v)\)
        \zi \(\proc{Vorgänger}(v) \gets v*\)
      \End
    \End
  \End
\end{codebox}

Der gesamte Block, der unter dem \enquote{while} eingerückt ist, wird
solange ausgeführt wie \(S \neq V\) ist oder mit anderen Worten, bis
zu jeder Ecke aus \(V\) ein kürzester Weg gefunden worden ist.

\begin{codebox}
  \zi finde \(v*\) aus \(V \setminus S\) mit
  \zi \(\quad \proc{Distanz}(v*) \gets \min\{\proc{Distanz}(v) : v \in V \setminus S\}\)
  \zi \(S \gets S \cup \{v*\}\)
\end{codebox}

Zu Beginn jeder Iteration wird zunächst eine bisher noch nicht
untersuchte Ecke \(v*\) gewählt, deren Distanz vom Startpunkt \(s\)
aus am geringsten ist und zum bisher bekannten kürzesten Weg \(S\)
hinzugefügt.

\begin{codebox}
  \zi \For \textbf{all} \(v \in V \setminus S\)
    \zi \Do
    \If \(\proc{Distanz}(v*) + \proc{Bogenlänge}(v*, v) < \proc{Distanz}(v)\)
      \zi \Then
      \(\proc{Distanz}(v) \gets \proc{Distanz}(v*) + \proc{Bogenlänge}(v*, v)\)
      \zi \(\proc{Vorgänger}(v) \gets v*\)
    \End
  \End
\end{codebox}

Nachdem ein \(v*\) gewählt worden ist, werden alle übrigen Ecken
durchgegangen und ein Update vorgenommen, da es sein kann, dass über
die zuletzt gewählte Ecke \(v*\) ein kürzerer Weg zu einer der anderen
Ecken gefunden wird als bisher bekannt.

Ist die Distanz von \(v*\) plus das Bogengewicht zwischen \(v*\) und
\(v\) größer als die Distanz von \(v\), so muss nichts unternommen
werden, da der Weg über \(v*\) nicht kürzer ist.  Andernfalls wird die
Distanz von \(v\) aktualisiert und der Vorgänger von \(v\) auf \(v*\)
gesetzt \autocite[siehe][73--74]{gri05}.

In
\autoref{fig:kuerzeste-wege-funktionsweise-des-dijkstra-algorithmus}
wird die Funktionsweise des Dijkstra-Algorithmus anhand eines
Beispielgraphen Schritt für Schritt erklärt.

\begin{figure}[h]
  \centering
  \foreach \i in {1,...,6} {
    \subcaptionbox{}[.32\textwidth]{\includegraphics{kuerzeste-wege-dijkstra-algorithmus-graph-\i}}
  }
  \captionsetup{format=plain}
  \caption[Funktionsweise des Dijkstra-Algorithmus]{Die Funktionsweise
    des Dijkstra-Algorithmus schrittweise erklärt.  Zu allen grau
    hinterlegten Ecken ist schon ein kürzester Weg gefunden worden,
    und durchgezogene Linien zeigen die Kanten an, über die der
    kürzeste Weg verläuft.  Die in den Knoten dargestellte Zahl
    entspricht der \proc{Distanz}\((v)\).  \textbf{(a)} zeigt den
    Zustand des Graphen bis zur while-Schleife in Zeile fünf.  Der
    \proc{Distanz}\((s)\) wurde der Wert \(0\) zugewiesen, und alle
    Distanzmarken wurden gesetzt.  Die Ecken \(c\) und \(t\) besitzen
    keine direkte Kante zu \(s\), weshalb ihre Distanzmarken auf
    unendlich gesetzt worden sind.  \textbf{(b)--(e)} stellen den
    Zustand nach jeder Iteration dar, bei der ein neues \(v*\) (grau
    hinterlegt) gewählt worden ist und alle Distanzmarken der noch
    nicht untersuchten Ecken aktualisiert worden sind.  Dieses Update
    ist bei der Ecke \(t\) besonders gut zu beobachten.  In
    \textbf{(f)} ist schließlich der kürzeste \(s\)-\(t\)-Weg
    \(P = \{s,b,c,t\}\) mit seiner Distanz \(\varphi(P) = 10\) zu
    sehen.}
  \label{fig:kuerzeste-wege-funktionsweise-des-dijkstra-algorithmus}
\end{figure}

Es kann auch interessant sein, mit Hilfe eines webbasierten Programms
die Funktionsweise zu visualisieren.  Besonders möchte der Autor die
interaktiven Anwendungen von \citefullauthor{vel14}
\autocite[siehe][]{vel14} und \citefullauthor{pan22}
\autocite[siehe][]{pan22} hervorheben.  Hier kann man zudem ähnliche
Algorithmen wie den A*-Algorithmus, den Bellman-Ford-Algorithmus und
den Floyd-Warshall-Algorithmus ausprobieren und vergleichen.

\section{Laufzeit des Dijkstra-Algorithmus}

Der Dijkstra-Algorithmus löst das SPP zwar nicht
unbedingt optimal, jedoch \textit{effizient}, also in annehmbarer
Zeit, ohne dass es zu einer kombinatorischen Explosion kommt.

Da die Laufzeit eines Algorithmus immer von der Größe der zu
verarbeitenden Datenmenge und der verwendeten Hardware abhängt, ist es
nicht sinnvoll, diese Laufzeit zum Beispiel in Millisekunden
anzugeben.  Aus diesem Grund teilt man Algorithmen mit Hilfe der
sogenannten \textit{\(O\)-Notation} (sprich \enquote{Big-Oh-Notation}) in
verschiedene Komplexitätsklassen ein.

Durch diese Klassen lässt sich abschätzen, wie sich die Laufzeit des
Algorithmus in Abhängigkeit zur Größe der Eingabe verhält
\autocite[siehe][47--48]{cor09}.

Der Dijkstra-Algorithmus fällt in die Komplexitätsklasse \(O(V^2)\),
er besitzt also eine quadratische Laufzeit\footnote{Um den Rahmen
  dieser Arbeit nicht zu sprengen, soll auf die Ermittlung dieses
  Wertes hier verzichtet werden.  In \textcite[89--104]{gri05} wird
  die Laufzeit des Dijkstra-Algorithmus Schritt für Schritt
  hergeleitet.}.

In \autoref{fig:kuerzeste-wege-laufzeiten} sind verschiedene
Komplexitätsklassen zu sehen.

\begin{figure}[h]
  \centering
  \begin{tikzpicture}
    \begin{axis}[axis lines=left,xmin=0,ymin=0,ymax=80,legend
      entries={\(O(V)\),\(O(V \log_2(V) + E)\),\(O(V^2)\),\(O(V^3)\)},
      xtick=\empty,ytick=\empty,xlabel=Größe der
      Datenmenge,ylabel=Laufzeit]

      \addplot{x};
      \addplot{x*log2(x) + x/2}; 
      \addplot{x^2};
      \addplot{x^3};
    \end{axis}
  \end{tikzpicture}
  \caption[Laufzeiten verschiedener Komplexitätsklassen]{Die
    Veränderung der Laufzeit verschiedener Komplexitätsklassen in
    Abhängigkeit zur Größe der Datenmenge.}
  \label{fig:kuerzeste-wege-laufzeiten}
\end{figure}

\section{Verbesserungen des Dijkstra-Algorithmus}
\label{sec:kuerzeste-wege-verbesserungen-des-dijkstra-algorithmus}

Mit Hilfe diverser Methoden ist es möglich, den Dijkstra-Algorithmus
zu verbessern und die Dauer zur Berechnung eines kürzesten Weges zu
minimieren.

Die erste Möglichkeit besteht darin, durch geeignete Datenstrukturen
die Laufzeit des Dijkstra-Algorithmus zu verkürzen.  Dadurch lässt er
sich sogar der Komplexitätsklasse \(O(V \log_2(V) + E)\) zuordnen, der
Algorithmus findet also bei gleicher Datenmenge schneller eine Lösung.
\citefullauthor{cor09} gehen in ihrem Buch \citetitle{cor09} darauf
genauer ein \cite[siehe][662]{cor09}.

Desweiteren ist es denkbar~-- sofern es die Aufgabenstellung
erlaubt~-- die Berechnung eines kürzesten Weges zu stoppen, sobald wir
an der Endecke~\(t\) angelangt sind.  Der Dijkstra-Algorithmus
benötigt lediglich eine kleine Anpassung der Schleife in Zeile fünf:

\begin{codebox}
  \zi \While \(t \notin S \qquad \) \Comment statt \(S \neq V\)
\end{codebox}

Gerade bei der Routenplanung kann es jedoch von Vorteil sein, den
kürzesten Weg zu allen Ecken zu kennen.

Durch Vertauschen der Ecken \(s\) und \(t\) berechnen wir kürzeste
Wege von der Endecke \(t\) zu allen Ecken \(v\) des Graphen, also
auch zur Startecke \(s\).

Verfahren wir uns auf dem Weg von \(s\) nach \(t\) und kommen von der
ursprünglichen Route ab, so landen wir an irgendeiner Ecke \(v\).  Das
Navi muss zum Glück den kürzesten Weg nicht neu berechnen, es kennt ja
schon einen kürzesten Weg von unserem jetzigen Standort, der Ecke
\(v\), zur Endecke~\(t\) und kann uns sofort die neue Strecke
vorschlagen.

Zu guter Letzt sei auch der A*-Algorithmus angemerkt.  Dieser ist eine
Verallgemeinerung und Erweiterung des Dijkstra-Algorithmus, die mit
Hilfe einer sogenannten \textit{Heuristik}, einer Schätzfunktion, zuerst die
Ecken untersucht, die \enquote{wahrscheinlich auf dem kürzesten Weg
  zum Ziel führen} \autocite[65]{bag20}.

Möchte man zum Beispiel auf dem schnellsten Weg von Penzberg nach
Nürnberg, so wäre es kontraproduktiv, auf die Autobahn in Richtung
Garmisch-Partenkirchen zu fahren, statt auf die in Richtung München.

Der A*-Algorithmus berücksichtigt dies und sucht dadurch sehr viel
zielgerichteter.  Zudem liefert er, im Gegensatz zum
Dijkstra-Algorithmus, immer die optimale Lösung für das SPP.

Dennoch ist der Dijkstra-Algorithmus keineswegs zu unterschätzen.
1995 gelang es dem Journalisten und c't-Autor \citefullauthor{bag20},
mittels eines 386er Prozessors und \qty{4}{\mega\byte}
Arbeitsspeicher\footnote{Heutzutage sind über \qty{4}{\giga\byte}, oft
  \qty{8}{\giga\byte} bis \qty{16}{\giga\byte} üblich.}, Routen
innerhalb ganz Europa zu berechnen.

Andere Algorithmen wie der Floyd-Warshall-Algorithmus sind zwar in der
Lage, auch auf Graphen mit Kanten negativen Gewichtes zu operieren,
fallen jedoch in die Komplexitätsklasse \(O(V^3)\) oder höher.  Sofern
möglich, wird deshalb der Dijkstra-Algorithmus verwendet.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../seminararbeit"
%%% End:
