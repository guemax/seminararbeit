\chapter{Anwendungen}
\label{chap:anwendungen}

Ist von \enquote{Kürzeste-Wege-Problemen} die Rede, so ist die erste
Assoziation meist die eines Navis oder einer Kartenapp, die einem eine
möglichst kurze Route zwischen zwei Orten berechnet.

Die Routenplanung ist jedoch nur ein kleiner Bruchteil dessen, was
sich alles mittels Algorithmen zur Lösung des SPP
anstellen lässt.  Durch geeignete Abstraktion lassen sich sehr viele
Probleme als Kürzeste-Wege-Problem beschreiben.

Einen (nicht vollständigen) Einblick in diese Anwendungen soll dieses
Kapitel bieten.

\section{Chinesisches-Postboten-Problem}
\label{sec:anwendungen-chinesisches-postboten-problem}

Das Chinesische-Postboten-Problem, 1962 durch den chinesischen
Mathematiker Mei-Ko Kwan eingeführt, ist eine Verallgemeinerung des
Eulerkreises \autocite[siehe][230]{gri05}.

Paketdienste müssen sicherstellen, dass abends jede Straße der Stadt
und somit jedes Haus abgearbeitet worden ist.  Bestenfalls beendet der
Postbote seine Schicht direkt am Verteilerzentrum, ohne vom letzten
Haus eine weite Strecke zum Depot zurücklegen zu müssen.  Idealerweise
besitzt die mathematische Repräsentation des Straßennetzes einer
Stadt demnach einen Eulerkreis.

Leider ist dies jedoch aufgrund von unter anderem Sackgassen, aus
denen man nicht ohne Doppelverwendung der Kante wieder herauskommt,
sehr unwahrscheinlich.

Sogenannte betriebsbedingte Überbrückungsfahrten umgehen dieses
Problem \autocite[siehe][212]{gri05}.  Damit bezeichnen wir Strecken,
die zurückgelegt werden müssen, um aus einer Sackgasse zu gelangen,
bei der jedoch keine Arbeit verrichtet wird (da hier die Post schon
ausgetragen worden ist).

Sobald alle betriebsbedingten Überbrückungsfahrten berechnet und als
Kanten zum ursprünglichen Graphen hinzugefügt worden sind, können wir
nach einem Eulerkreis suchen \autocite[siehe][231]{gri05}.

Nicht nur Paketdienste, auch Entsorgungsunternehmen, Räumdienste und
die Straßenreinigung profitieren von geschlossenen Routen und
berechnen hierfür einen kürzesten Eulerkreis
\autocite[siehe][230]{gri05}.

\section{Problem des Handlungsreisenden}

Im vorherigen Anwendungsbeispiel haben wir uns mit der Suche nach
einem kürzesten Eulerkreis beschäftigt.  Beim Problem des
Handlungsreisenden, auch Rundreiseproblem oder
Traveling-Salesman-Problem (TSP) genannt, geht es nun darum, einen
kürzesten Hamiltonkreis zu finden \autocite[siehe][289]{gri05}.

Der amerikanische Mathematiker George Dantzig war es, der 1947 die
schnellste Methode entwickelte, um das in den vierziger Jahren immer
populärer gewordene TSP zu lösen.

Während seines Studiums kam Dantzig zu spät zu einer Vorlesung.  Wie
gewohnt hatte sein Professor die bis zur nächsten Stunde zu
erledigenden Aufgaben bereits an die Tafel geschrieben, und Dantzig
machte sich sofort zu Hause daran, diese zu bearbeiten.

Auch wenn die Aufgaben ihm etwas schwieriger als üblich vorkamen, gab
der Student nicht auf und legte dem Professor schließlich die gelösten
Aufgaben zur Verbesserung vor.

Die Pointe?  Ausnahmsweise standen dieses Mal keine Hausaufgaben an
der Tafel, sondern \enquote{zwei ungelöste Probleme, an denen sich
  bisher einige der besten Wissenschaftler die Zähne ausgebissen
  hatten} \autocite[348]{gri05}.

In den darauffolgenden Jahren wurden die Algorithmen immer weiter
verbessert und mit Hilfe von Computern immer größere Routen berechnet.

Während George Dantzig, Ray Fulkerson und Selmer Johnson 1954 ein
Rundreiseproblem durch die \enquote{nur} 49 US-Hauptstädte und somit
in einem Graphen mit 49 Ecken lösten, erhöhte Martin Grötschel 1977
den Rekord auf 120 Ecken und 1987 zusammen mit Olaf Holland sogar auf
666.

2004 lag der Weltrekord bei einer Tour durch alle \num{24978}
schwedischen Ortschaften~\autocite[siehe][351--352]{gri05}.

In der Industrie kommt das TSP unter anderem bei der Bohrung oder
Verdrahtung von Platinen zum Einsatz.

\section{Rekonstruktion einer möglichen Route}

Zu guter Letzt soll noch ein außergewöhnlicher Fall der Routenplanung,
der sogar für Schlagzeilen sorgte, erwähnt werden.

Erst März diesen Jahres gelang es Geoinformatikern, einen Mordfall mit
Hilfe eines Routingalgorithmus aufzuklären.

Seit dem 7. März 2023 wurde die 19-jährige Kezhia aus Klötze in
Sachsen-Anhalt vermisst.  Ihr Freund hatte ihr Verschwinden erst nach
drei Tagen der Polizei gemeldet, er habe sie bei einer Freundin in
Wolfsburg abgesetzt, abends jedoch nicht mehr angetroffen
\autocite[siehe][134]{gra23}.

Nachdem diese Aussage der Polizei verdächtig erschien, begann sie,
gegen den Freund zu ermitteln.  Im Firmenwagen, mit dem der Mann seine
Freundin abgesetzt haben will, entdeckten die Ermittler einen
Tachographen.  Dieser speichert sekundengenau die Geschwindigkeit des
Wagens, im Gegensatz zu anderen Modellen jedoch nicht den Ort des
Fahrzeugs, da er baubedingt kein GPS besitzt
\autocite[siehe][135]{gra23}.

Daraufhin wandte sich die Polizei an Thomas Brinkhoff und sein Team
von der niederländischen Jade-Hochschule in Oldenburg.  Die
Geoinformatiker entwickelten ein interaktives Programm, das basierend
auf den Daten des Tachographen mögliche Routen des Tatverdächtigen
berechnete.

Ein Großteil der Ergebnisse ließ sich ausschließen: Plötzliche Stopps
auf der Autobahn oder einer Schnellstraße und hohe Geschwindigkeiten
in einer Kurve waren ein guter Indikator für eine unwahrscheinliche
Route.  Zudem konnte der Streckenverlauf mit Hilfe einer
Öffentlichkeitsfahndung immer weiter eingegrenzt werden
\autocite[siehe][135--136]{gra23}.

Eine der Routen zeigte einen auffälligen Halt in der Nähe eines
Kieswerkes.  Die Polizei leitete eine Suche in besagtem Gebiet ein und
fand die Leiche der Vermissten in einer zwei Meter tiefen Grube.

Ohne die Hilfe und genauen Hinweise der Wissenschaftler wäre es nahezu
unmöglich gewesen, die Frau 41 Kilometer von ihrem Wohnort entfernt zu
finden und die Schuld des Mannes zu beweisen
\autocite[siehe][136--137]{gra23}.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../seminararbeit"
%%% End:
